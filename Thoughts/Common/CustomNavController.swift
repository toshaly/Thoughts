//
//  CustomNavController.swift
//  Thoughts
//
//  Created by Anton Debelyy on 29.11.2022.
//

import Foundation
import UIKit

class CustomNavController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.prefersLargeTitles = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.navigationBarTap))
        tap.numberOfTapsRequired = 1
        self.navigationBar.addGestureRecognizer(tap)


    }
    
    @objc private func navigationBarTap() {
        view.endEditing(true)
    }
}

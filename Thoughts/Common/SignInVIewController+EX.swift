//
//  SignInVIewController+EX.swift
//  Thoughts
//
//  Created by Anton Debelyy on 29.11.2022.
//

import Foundation
import UIKit

extension SignInViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

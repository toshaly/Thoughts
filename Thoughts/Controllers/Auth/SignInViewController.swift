//
//  SignInViewController.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import UIKit

class SignInViewController: UITabBarController {
    
    private lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.bounces = true
        view.showsVerticalScrollIndicator = true
        return view
    }()
    
    private lazy var contentView = SignInScrollContentView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
        self.configController()
        self.hideKeyboardWhenTappedAround()
        self.view.addSubview(self.scrollView)
        self.scrollView.addSubview(self.contentView)
        self.changeViewWhenKeyboardApear()
//        let heightConstraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
//            heightConstraint.priority = UILayoutPriority(250)
        
        
        NSLayoutConstraint.activate([
            self.scrollView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            self.scrollView.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            self.contentView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor),
            self.contentView.topAnchor.constraint(equalTo: self.scrollView.topAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor),
        ])
    }
    
    private func configController() {
        self.title = "Sign In"
        self.view.backgroundColor = .systemBackground
        self.navigationItem.largeTitleDisplayMode = .always
    }
    
    private func changeViewWhenKeyboardApear() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification:NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    @objc private func keyboardWillHide(_ notification:NSNotification) {
        self.scrollView.contentInset = UIEdgeInsets.zero
    }
    
    @objc private func didTapSignIn(){
        
    }
    
    @objc private func didTapCreateAcc(){
        
    }
}

//
//  ProfileVIewControllerViewController.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemBackground
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign out", style: .done, target: self, action: #selector(didTapSignOut))
    }
    
 @objc private func didTapSignOut() {
     
    }

}

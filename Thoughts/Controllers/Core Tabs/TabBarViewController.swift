//
//  TabBarViewController.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpControllers()
        
    }
   private func setUpControllers(){
        let home = HomeViewController()
        home.title = "Home"
        home.navigationItem.largeTitleDisplayMode = .always
        
        let prof = ProfileViewController()
        prof.title = "Profile"
        prof.navigationItem.largeTitleDisplayMode = .always
        
        let homeNav = UINavigationController(rootViewController: home)
        homeNav.navigationBar.prefersLargeTitles = true
        homeNav.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), tag: 1)
        let profNav = UINavigationController(rootViewController: prof)
        profNav.navigationBar.prefersLargeTitles = true
        profNav.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person.circle"), tag: 2)
    
        setViewControllers([homeNav, profNav], animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

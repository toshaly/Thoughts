//
//  AuthManager.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import Foundation
import FirebaseAuth

final class AuthManager {
    
    static let shared = AuthManager()
    
    private let auth = Auth.auth()
    
    private init() {}
        
    public var isSignedIn: Bool {
        return auth.currentUser != nil
    }
        
    public func singUp(_ email: String, _ password: String, _ completion: @escaping (Bool) -> Void ) {
        
        guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
              !password.trimmingCharacters(in: .whitespaces).isEmpty,
              password.count >= 8 else { return }
        
        auth.createUser(withEmail: email, password: password) { result, error in
            guard result != nil, error == nil else {
                completion(false)
                return
            }
            completion(true)
        }
    }
    
    public func singIn(_ email: String, _ password: String, _ completion: @escaping (Bool) -> Void ) {
        
        guard !email.trimmingCharacters(in: .whitespaces).isEmpty,
              !password.trimmingCharacters(in: .whitespaces).isEmpty,
              password.count >= 8 else { return }
        
        auth.signIn(withEmail: email, password: password) { result, error in
            guard result != nil, error == nil else {
                completion(false)
                return
            }
            completion(true)
        }
    }
    
    public func singOut(_ completion: (Bool) -> Void ) {
        
        do {
            try auth.signOut()
            completion(false)
        }
        catch {
            print(error)
            completion(false)
        }
        
    }

}

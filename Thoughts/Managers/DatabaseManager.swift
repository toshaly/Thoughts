//
//  DataBaseManager.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import Foundation
import FirebaseFirestore

final class DatabaseManager {
    
    static let shared = DatabaseManager()
    
    private let container = Firestore.firestore()
    
    private init() {}
    
    public func create(_ post: BlogPost, _ user: User, _ completion: @escaping (Bool) -> Void) {
        
    }
    
    public func getAllPosts(_ completion: @escaping ([BlogPost]) -> Void) {
        
    }
    
    public func getPostsForUser(_ user: User, _ completion: @escaping ([BlogPost]) -> Void) {
        
    }
    
    public func insert(_ user: User, _ completion: @escaping (Bool) -> Void) {
        
    }
}

//
//  StorageManager.swift
//  Thoughts
//
//  Created by Anton Debelyy on 09.11.2022.
//

import Foundation
import FirebaseStorage

final class StorageManager {
    
    static let shared = StorageManager()
    
    private let container = Storage.storage().reference()
    
    private init() {}
    
    public func uploadUserPicture(_ email: String, image: UIImage?, _ completion: @escaping (Bool) -> Void) {
        
    }
    
    public func downloadURLForProfilePict(_ user: User, _ completion: (URL?) -> Void) {
        
    }
    
    public func uploadBlogHeaderImage(_ post: BlogPost, image: UIImage?, _ completion: @escaping (Bool) -> Void) {
        
    }
    
    public func downloadURLForPostHeader(_ post: BlogPost, _ completion: (URL?) -> Void) {
        
    }
    
}

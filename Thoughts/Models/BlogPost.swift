//
//  BlogPost.swift
//  Thoughts
//
//  Created by Anton Debelyy on 11.11.2022.
//

import Foundation

struct BlogPost {
    let id: String
    let title: String
    let timestamp: TimeInterval
    let text: String
    let headerImageURL: URL?
}

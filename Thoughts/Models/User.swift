//
//  User.swift
//  Thoughts
//
//  Created by Anton Debelyy on 11.11.2022.
//

import Foundation

struct User {
    let name: String
    let email: String
    let profilePictuteURL: URL?
}

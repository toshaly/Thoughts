//
//  SignInContentView.swift
//  Thoughts
//
//  Created by Anton Debelyy on 28.11.2022.
//

import UIKit

class SignInContentView: UIView {
    
    private let emailField: TextFieldWithPadding = {
        let field = TextFieldWithPadding()
        field.keyboardType = .emailAddress
        field.backgroundColor = .secondarySystemBackground
        field.placeholder = "Enter your email"
        field.layer.cornerRadius = 10
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    private let passwordField: TextFieldWithPadding = {
        let field = TextFieldWithPadding()
        field.backgroundColor = .secondarySystemBackground
        field.placeholder = "Enter your password"
        field.isSecureTextEntry = true
        field.layer.cornerRadius = 10
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()
    
    private let signInButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 10
        button.backgroundColor = .systemBlue
        button.setTitle("Sign in", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let createAccButton: UIButton = {
        let button = UIButton()
        button.setTitle("Create Account", for: .normal)
        button.setTitleColor(.link, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()
    
    private let inputsStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 20
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.inputsStack.addArrangedSubview(self.emailField)
        self.inputsStack.addArrangedSubview(self.passwordField)
        self.inputsStack.addArrangedSubview(self.signInButton)
        self.inputsStack.addArrangedSubview(self.createAccButton)
        self.addSubview(self.inputsStack)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        NSLayoutConstraint.activate([self.inputsStack.topAnchor.constraint(equalTo: self.topAnchor),
                                     self.inputsStack.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                                     self.inputsStack.rightAnchor.constraint(equalTo: self.rightAnchor),
                                     self.inputsStack.leftAnchor.constraint(equalTo: self.leftAnchor)])
        super.updateConstraints()
    }
    
}

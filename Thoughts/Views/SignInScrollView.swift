//
//  SignInHeaderView.swift
//  Thoughts
//
//  Created by Anton Debelyy on 23.11.2022.
//

import UIKit

class SignInScrollContentView: UIView {
    
    private let imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.backgroundColor = .systemBackground
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 28, weight: .medium)
        label.text = "Join our community✈️"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let contentView: SignInContentView = {
        let view = SignInContentView()
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        self.addSubview(label)
        self.addSubview(contentView)
    
    }
                                         
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            self.imageView.heightAnchor.constraint(equalToConstant: 150),
            self.imageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 30),
            self.imageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -30)
        ])
        NSLayoutConstraint.activate([
            self.label.topAnchor.constraint(equalTo: self.imageView.bottomAnchor),
            self.label.heightAnchor.constraint(equalToConstant: 50),
            self.label.leftAnchor.constraint(equalTo: self.imageView.leftAnchor),
            self.label.rightAnchor.constraint(equalTo: self.imageView.rightAnchor)
        ])
        NSLayoutConstraint.activate([
            self.contentView.topAnchor.constraint(equalTo: self.label.bottomAnchor, constant: 50),
            self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.contentView.leftAnchor.constraint(equalTo: self.imageView.leftAnchor),
            self.contentView.rightAnchor.constraint(equalTo: self.imageView.rightAnchor)
        ])
        super.updateConstraints()
    }
}
